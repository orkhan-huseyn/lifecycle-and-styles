import React from 'react';

class HelloWorld extends React.Component {
  constructor(props) {
    super(props);
    console.log('[HelloWorld] constructor called');
  }

  componentDidMount() {
    console.log('[HelloWorld] componentDidMount called');
    this.intervalId = setInterval(() => {
      console.log('[HelloWorld] timer is working');
    }, 1000);
  }

  componentDidUpdate() {
    console.log('[HelloWorld] componentDidUpdate called');
  }

  componentWillUnmount() {
    console.log('[HelloWorld] componentWillUnmount called');
    clearInterval(this.intervalId);
  }

  render() {
    console.log('[HelloWorld] render called');
    return <h1>Yeah! Counter is 5! 😍</h1>;
  }
}

export default HelloWorld;
