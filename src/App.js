import React from 'react';
import ActionButton from './components/ActionButton';
import PrimaryButton from './components/PrimaryButton';

class App extends React.Component {
  render() {
    return (
      <div className="container">
        <ActionButton />
        <PrimaryButton />
      </div>
    );
  }
}

export default App;
