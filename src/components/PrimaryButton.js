import React from 'react';
import classes from './PrimaryButton.module.css';

class PrimaryButton extends React.Component {
  render() {
    return <button className={classes.btn}>Primary button</button>;
  }
}

export default PrimaryButton;
