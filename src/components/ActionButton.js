import React from 'react';
import './ActionButton.css';

class ActionButton extends React.Component {
  render() {
    return <button className="btn">Action button</button>;
  }
}

export default ActionButton;
