import React from 'react';
import HelloWorld from './HelloWorld';

class CounterExample extends React.Component {
  constructor(props) {
    super(props);
    console.log('[CounterExample] constructor called');
    this.state = {
      counter: 0,
    };

    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    console.log('[CounterExample] componentDidMount called');
  }

  shouldComponentUpdate() {
    console.log('[CounterExample] shouldComponentUpdate called');
    return false;
  }

  componentDidUpdate() {
    console.log('[CounterExample] componentDidUpdate called');
  }

  handleClick() {
    const { counter } = this.state;
    this.setState({
      counter: counter + 1
    });
  }

  render() {
    console.log('[CounterExample] render called');
    const { counter } = this.state;

    return (
      <>
        <h1>Counter is {counter}</h1>
        <button onClick={this.handleClick}>Click me</button>
        {
          counter === 5 ? <HelloWorld /> : null
        }
      </>
    );
  }
}

export default CounterExample;
